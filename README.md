# TopQuiz

## Description

Créer un jeu de type QCM sous Android (cours Open Classrooms).  

La base de l'application est implémentée tout au long du cours avec plus ou moins de liberté. 
L'activité finale consiste à améliorer celle-ci en ajoutant un historique des scores.  

## Versions

  - **version 0.2 :** gestion de l'historique du score des joueurs (tri par score ou login)
  - **version 0.1 :** base de l'application (QCM, affichage du score et récupération des précédentes informations)

## Source

[Activité "votre première application Android"](http://exercices.openclassrooms.com/assessment/662?id=4517166&slug=developpez-votre-premiere-application-android&login=416216&tk=e43191e47357bd561d32a19535350b9c&sbd=2016-02-01&sbdtk=2466d6bae51e373d89ac8e3f74213199)  
