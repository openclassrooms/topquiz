package local.workstation.topquiz.controler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.Collections;
import java.util.List;

import local.workstation.topquiz.R;
import local.workstation.topquiz.adapter.ScoreAdapter;
import local.workstation.topquiz.model.Score;
import local.workstation.topquiz.model.ScoreHistory;

import static local.workstation.topquiz.controler.MainActivity.PREF_KEY_SCORES;
import static local.workstation.topquiz.model.Score.ScoreByLoginComparator;

public class ScoresActivity extends AppCompatActivity {
    private SharedPreferences mPreferences;

    private ListView mListView;
    private Button mSortByScoreButton;
    private Button mSortByLoginButton;

    private List<Score> mScoreList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        mPreferences = getApplicationContext().getSharedPreferences("TOPQUIZ", MODE_PRIVATE);

        String scores = mPreferences.getString(PREF_KEY_SCORES, null);
        ScoreHistory tmpScoreHistory;

        mSortByScoreButton = findViewById(R.id.sort_by_score);
        mSortByLoginButton = findViewById(R.id.sort_by_login);

        mSortByScoreButton.setEnabled(false);
        mSortByLoginButton.setEnabled(true);

        if (scores != null && scores != "") {
            tmpScoreHistory = new ScoreHistory(ScoreHistory.fromString(scores));
            mScoreList = tmpScoreHistory.getScoreList();

            mListView = findViewById(R.id.list);
            mListView.setAdapter(new ScoreAdapter(this, mScoreList));
        } else {
            finish();
        }

        mSortByScoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSortByLoginButton.setEnabled(true);
                mSortByScoreButton.setEnabled(false);
                Collections.sort(mScoreList, Collections.reverseOrder());
                mListView.setAdapter(new ScoreAdapter(getBaseContext(), mScoreList));
            }
        });

        mSortByLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSortByScoreButton.setEnabled(true);
                mSortByLoginButton.setEnabled(false);
                Collections.sort(mScoreList, ScoreByLoginComparator);
                mListView.setAdapter(new ScoreAdapter(getBaseContext(), mScoreList));
            }
        });
    }
}
