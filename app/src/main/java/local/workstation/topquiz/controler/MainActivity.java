package local.workstation.topquiz.controler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import local.workstation.topquiz.R;
import local.workstation.topquiz.model.Score;
import local.workstation.topquiz.model.ScoreHistory;
import local.workstation.topquiz.model.User;

public class MainActivity extends AppCompatActivity {

    private static final int GAME_ACTIVITY_REQUEST_CODE = 1;
    public static final String PREF_KEY_SCORE = "PREF_KEY_SCORE";
    public static final String PREF_KEY_FIRSTNAME = "PREF_KEY_FIRSTNAME";
    public static final String PREF_KEY_SCORES = "PREF_KEY_SCORES";

    private SharedPreferences mPreferences;

    private TextView mTextView;
    private EditText mEditText;
    private Button mButton;
    private Button mScores;

    private User mUser;

    private void alreadyPlayed() {
        String firstName = mPreferences.getString(PREF_KEY_FIRSTNAME, null);

        if (firstName != null) {
            int score = mPreferences.getInt(PREF_KEY_SCORE, 0);
            String welcomeBack = "Welcome back " + firstName + "!";
            welcomeBack += " Your last score was " + score;

            mEditText.setText(firstName);
            mEditText.setSelection(firstName.length());
            mTextView.setText(welcomeBack);
            mButton.setEnabled(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (GAME_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            int score = data.getIntExtra(GameActivity.BUNDLE_EXTRA_SCORE, 0);
            String login = mPreferences.getString(PREF_KEY_FIRSTNAME, null);

            mPreferences.edit().putInt(PREF_KEY_SCORE, score).apply();

            alreadyPlayed();

            if (login != null && login != "") {
                // adding score + login in score history
                String scores = mPreferences.getString(PREF_KEY_SCORES, null);
                ScoreHistory tmpScoreHistory;

                if (scores != null && scores != "") {
                    tmpScoreHistory = new ScoreHistory(ScoreHistory.fromString(scores));
                    tmpScoreHistory.add(new Score(login, score));
                } else {
                    List<Score> tmpScore = new ArrayList<>();
                    tmpScore.add(new Score(login, score));
                    tmpScoreHistory = new ScoreHistory(tmpScore);
                }

                mPreferences.edit().putString(PREF_KEY_SCORES, tmpScoreHistory.toString()).apply();
                mScores.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPreferences = getSharedPreferences("TOPQUIZ", MODE_PRIVATE);

        mTextView = findViewById(R.id.textView);
        mEditText = findViewById(R.id.editText);
        mButton = findViewById(R.id.button);
        mScores = findViewById(R.id.scores);

        mButton.setEnabled(false);
        mScores.setVisibility(View.INVISIBLE);

        mUser = new User();

        alreadyPlayed();

        String tmp = mPreferences.getString(PREF_KEY_SCORES, null);

        if (tmp != null && !tmp.isEmpty())
            mScores.setVisibility(View.VISIBLE);

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mButton.setEnabled(s.toString().length() != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUser.setFirstName(mEditText.getText().toString());
                mPreferences.edit().putString(PREF_KEY_FIRSTNAME, mUser.getFirstName()).apply();

                Intent gameActivity = new Intent(MainActivity.this, GameActivity.class);
                startActivityForResult(gameActivity, GAME_ACTIVITY_REQUEST_CODE);
            }
        });

        mScores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scoresActivity = new Intent(MainActivity.this, ScoresActivity.class);
                startActivity(scoresActivity);
            }
        });
    }
}
