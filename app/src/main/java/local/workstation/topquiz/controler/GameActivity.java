package local.workstation.topquiz.controler;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import local.workstation.topquiz.R;
import local.workstation.topquiz.model.Question;
import local.workstation.topquiz.model.QuestionBank;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String BUNDLE_EXTRA_SCORE = "BUNDLE_EXTRA_SCORE";
    public static final String BUNDLE_STATE_SCORE = "BUNDLE_STATE_SCORE";
    public static final String BUNDLE_STATE_QUESTION = "BUNDLE_STATE_QUESTION";

    private TextView mQuestionTextView;
    private Button mAnswerButton1;
    private Button mAnswerButton2;
    private Button mAnswerButton3;
    private Button mAnswerButton4;

    private QuestionBank mQuestionBank;
    private Question mCurrentQuestion;
    private int mNumberOfQuestion;
    private int mScore;

    private boolean mEnableTouchEvents;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(BUNDLE_STATE_SCORE, mScore);
        outState.putInt(BUNDLE_STATE_QUESTION, mNumberOfQuestion);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mQuestionBank = generateQuestionBank();
        mCurrentQuestion = mQuestionBank.getQuestion();

        if (savedInstanceState != null) {
            mNumberOfQuestion = savedInstanceState.getInt(BUNDLE_STATE_QUESTION);
            mScore = savedInstanceState.getInt(BUNDLE_STATE_SCORE);
        } else {
            mNumberOfQuestion = 4;
            mScore = 0;
        }

        mQuestionTextView = findViewById(R.id.textView2);
        mAnswerButton1 = findViewById(R.id.choice1);
        mAnswerButton2 = findViewById(R.id.choice2);
        mAnswerButton3 = findViewById(R.id.choice3);
        mAnswerButton4 = findViewById(R.id.choice4);

        mAnswerButton1.setOnClickListener(this);
        mAnswerButton2.setOnClickListener(this);
        mAnswerButton3.setOnClickListener(this);
        mAnswerButton4.setOnClickListener(this);

        mAnswerButton1.setTag(0);
        mAnswerButton2.setTag(1);
        mAnswerButton3.setTag(2);
        mAnswerButton4.setTag(3);

        mEnableTouchEvents = true;

        displayQuestion(mCurrentQuestion);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return mEnableTouchEvents && super.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View v) {
        if (mCurrentQuestion.getAnswerIndex() == (int) v.getTag()) {
            Toast.makeText(this, "Correct answer!", Toast.LENGTH_SHORT).show();
            mScore++;
        } else {
            Toast.makeText(this, "Wrong answer!", Toast.LENGTH_SHORT).show();
        }

        mEnableTouchEvents = false;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mEnableTouchEvents = true;

                if (--mNumberOfQuestion == 0)
                    endGame();

                mCurrentQuestion = mQuestionBank.getQuestion();
                displayQuestion(mCurrentQuestion);
            }
        }, 2000);
    }

    private QuestionBank generateQuestionBank() {
        List<Question> questionList = new ArrayList<>();

        questionList.add(new Question("Who is the creator of the Linux kernel?",
                Arrays.asList("Linus Torvalds", "Bill Gates", "Steve Wozniak", "John von Neumann"),
                0));
        questionList.add(new Question("Who is considered as the founding father of the web?",
                Arrays.asList("Alan Turing", "Tim Berners-Lee", "Mark Zuckerberg", "Mark Shuttleworth"),
                1));
        questionList.add(new Question("When was officially born the World Wide Web?",
                Arrays.asList("1952", "1967", "2004", "1990"),
                3));
        questionList.add(new Question("Who is Tux (The official mascot of the Linux kernel)?",
                Arrays.asList("A penguin", "A gnu", "An apple", "A fox"),
                0));
        questionList.add(new Question("Who launches the GNU project?",
                Arrays.asList("Steve Jobs", "Paul Allen", "Richard Stallman", "Linus Torvalds"),
                2));

        return new QuestionBank(questionList);
    }

    private void displayQuestion(final Question question) {
        mQuestionTextView.setText(question.getQuestion());
        mAnswerButton1.setText(question.getChoiceList().get(0));
        mAnswerButton2.setText(question.getChoiceList().get(1));
        mAnswerButton3.setText(question.getChoiceList().get(2));
        mAnswerButton4.setText(question.getChoiceList().get(3));
    }

    private void endGame() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Well Done!");
        builder.setMessage("Your score is " + mScore);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.putExtra(BUNDLE_EXTRA_SCORE, mScore);
                setResult(RESULT_OK, intent);

                finish();
            }
        });
        builder.create();
        builder.show();
    }
}
