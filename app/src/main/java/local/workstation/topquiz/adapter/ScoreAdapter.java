package local.workstation.topquiz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import local.workstation.topquiz.R;
import local.workstation.topquiz.model.Score;

public class ScoreAdapter extends BaseAdapter {
    private Context mContext;
    private List<Score> mScoreList;
    private LayoutInflater mLayoutInflater;

    public ScoreAdapter(Context context, List<Score> scoreList) {
        mContext = context;
        mScoreList = scoreList;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mScoreList.size();
    }

    @Override
    public Object getItem(int position) {
        return mScoreList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(R.layout.adapter_score, null);

        Score currentScore = (Score) getItem(position);
        String login = currentScore.getLogin();
        int score = currentScore.getScore();

        TextView loginView = convertView.findViewById(R.id.item_login);
        TextView scoreView = convertView.findViewById(R.id.item_score);

        loginView.setText(login);
        scoreView.setText(String.valueOf(score));

        return convertView;
    }
}
