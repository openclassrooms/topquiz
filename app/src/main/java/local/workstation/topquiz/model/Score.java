package local.workstation.topquiz.model;

import java.util.Comparator;

public class Score implements Comparable<Score> {
    private String mLogin;
    private int mScore;

    public Score(String login, int score) {
        mLogin = login;
        mScore = score;
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    public void setScore(int score) {
        mScore = score;
    }

    public String getLogin() {
        return mLogin;
    }

    public int getScore() {
        return mScore;
    }

    @Override
    public String toString() {
        return  mLogin + ":" + mScore;
    }

    @Override
    public int compareTo(Score o) {
        return getScore() - o.getScore();
    }

    public static Comparator<Score> ScoreByLoginComparator
            = new Comparator<Score>() {

        public int compare(Score o1, Score o2) {

            String score1 = o1.getLogin().toUpperCase();
            String score2 = o2.getLogin().toUpperCase();

            //ascending order
            return score1.compareTo(score2);

            //descending order
            //return score2.compareTo(score1);
        }

    };
}
