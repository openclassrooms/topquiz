package local.workstation.topquiz.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static local.workstation.topquiz.model.Score.ScoreByLoginComparator;

public class ScoreHistory {
    private List<Score> mScoreList;
    private int mMaxSize;

    public ScoreHistory(List<Score> scoreList, int maxSize) {
        setMaxSize(maxSize);
        mScoreList = scoreList;
        check();
    }

    public ScoreHistory(List<Score> scoreList) {
        this(scoreList, 5);
    }

    public int getMaxSize() {
        return mMaxSize;
    }

    public void setMaxSize(int maxSize) {
        if (maxSize < 0 || maxSize > 100)
            throw new IllegalArgumentException("invalid maxSize value");
        mMaxSize = maxSize;
    }

    public List<Score> getScoreList() {
        return mScoreList;
    }

    public List<Score> getScoreListSortByLogin() {
        List<Score> tmp = mScoreList;
        Collections.sort(tmp, ScoreByLoginComparator);

        return tmp;
    }

    public void add(Score score) {
        mScoreList.add(score);
        check();
    }

    public static List<Score> fromString(String history) {
        List<Score> scoreList = new ArrayList<>();

        String[] tmpScoreList = history.split(";");
        int size = tmpScoreList.length;
        for (int i = 0; i < size; i++) {
            String[] score = tmpScoreList[i].split(":");
            scoreList.add(new Score(score[0], Integer.parseInt(score[1])));
        }

        return scoreList;
    }

    @Override
    public String toString() {
        String tmpString = "";

        int size = mScoreList.size();
        for (int i = 0; i < size; i++) {
            if (i == size -1)
                tmpString += mScoreList.get(i);
            else
                tmpString += mScoreList.get(i) + ";";
        }

        return tmpString;
    }

    private void check() {
        Collections.sort(mScoreList, Collections.reverseOrder());
        while (mScoreList.size() > mMaxSize)
            mScoreList.remove(mScoreList.size() - 1);
    }

}
